package com.wishbone.moves;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.pieces.Piece;

/**
 * {@inheritDoc}
 * 
 * This class adds the castling rights and the positions
 * that a rook moved to and from.
 * 
 * @author Ross
 */
public class CastlingMove extends Move {

	private Piece rook;
	private Position rookFrom;
	private long castling;

	public CastlingMove(Piece piece, Position to, Position from, long castlingMoves,
			Piece rook, Position rookFrom) {
		super(piece, to, from);
		this.castling = castlingMoves;
		this.rook = rook;
		this.rookFrom = rookFrom;
	}
	
	@Override
	public void undoMove() {
		
		//move the rook back
		rook.internalMove(rookFrom);
		
		//reset the castling rights
		Board.getInstance().setCastlingMoves(castling);
		
		//moves the piece back
		super.undoMove();
	}
	
	@Override
	public String toString() {		
		return piece.toString() + " castled " + from + to;
	}

	public long getCastling() {
		return castling;
	}
}
