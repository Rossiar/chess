package com.wishbone.moves;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.pieces.Piece;

/**
 * {@inheritDoc}
 * 
 * This class adds the value of the piece captured
 * and the position of the capture.
 * 
 * @author Ross
 */
public class EnpassantCaptureMove extends CaptureMove {

	public EnpassantCaptureMove(Piece piece, Position to, Position from, Piece capture) {
		super(piece, to, from, capture);
	}
	
	@Override
	public void undoMove() {
		
		//reset enpassant capture square
		Board.getInstance().setEnpassantTarget(this.to);
		
		//moves the piece back and restore capture
		super.undoMove();
	}
	
	@Override
	public String toString() {
		return super.toString();
	}

}
