package com.wishbone.moves;

/**
 * Used to represent a failed move
 * 
 * @author Ross
 */
public class IllegalMove extends Move {
	
	public IllegalMove()
	{
		super(null, null, null);
	}

}
