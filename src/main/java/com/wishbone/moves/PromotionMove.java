package com.wishbone.moves;

import java.util.ArrayList;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.pieces.Piece;

/**
 * {@inheritDoc}
 * 
 * This class adds the piece that is to be
 * substituted for the pawn.
 * 
 * @author Ross
 */
public class PromotionMove extends Move {
	
	private Piece promotion;

	public PromotionMove(Piece piece, Position to, Position from, Piece promotion) {
		super(piece, to, from);
		this.promotion = promotion;
	}
	
	@Override
	public void undoMove() {
		ArrayList<Piece> pieces =  Board.getInstance().pieces;
		
		//remove promotion
		pieces.remove(promotion);
		
		//restore pawn
		pieces.add(piece);
		
		//move the pawn back
		super.undoMove();
	}

	@Override
	public String toString() {
		return piece.toString() + " promoted at " + from + to;
	}
}
