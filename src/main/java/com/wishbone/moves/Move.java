package com.wishbone.moves;

import com.wishbone.enums.Position;
import com.wishbone.pieces.Piece;

/**
 * Represents a move made on the board.
 * 
 * @author Ross
 */
public class Move implements Comparable<Move> {

	protected Piece piece;
	protected Position to;
	protected Position from;

	private static final double[] SQUARE_VALUES = {
		0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
		0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0,
		0.0, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.0,
		0.0, 0.1, 0.2, 0.3, 0.3, 0.2, 0.1, 0.0,
		0.0, 0.1, 0.2, 0.3, 0.3, 0.2, 0.1, 0.0,
		0.0, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.0,
		0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0,
		0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
	};
	
	public Move(Piece piece, Position to, Position from) {
		this.piece = piece;
		this.to = to;
		this.from = from;
	}
	
	public void undoMove() {
		//move the piece back
		piece.internalMove(from);
	}

	@Override
	public String toString() {
		return piece.toString() + " " + from + to;
	}

	public int compareTo(Move o) {
		double score = this.getScore();
		double oScore = o.getScore();

		if (score > oScore) {
			return 1;
		} else if (score < oScore) {
			return -1;
		}

		return 0;
	}

	
	public double getScore() {
		return SQUARE_VALUES[to.getPosition()] - SQUARE_VALUES[from.getPosition()];
	}

	public Piece getPiece() {
		return piece;
	}

	public int getTo() {
		return to.getPosition();
	}

	public int getFrom() {
		return from.getPosition();
	}

}
