package com.wishbone.moves;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.pieces.Piece;

/**
 * {@inheritDoc}
 * 
 * This class records the enpassant square that was before
 * this enpassant move.
 * 
 * @author Ross
 */
public class EnpassantMove extends Move {
	
	private Position enpassant;

	public EnpassantMove(Piece piece, Position to, Position from, Position enpassant) {
		super(piece, to, from);
		this.enpassant = enpassant;
	}
	
	@Override
	public void undoMove() {
		//restore enpassant target
		Board.getInstance().setEnpassantTarget(this.enpassant);
		
		//move the piece back
		super.undoMove();
	}

	public Position getEnpassant() {
		return this.enpassant;
	}

}
