package com.wishbone.moves;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.pieces.Piece;

public class CaptureMove extends Move {
	
	private Piece capture;
	
	public CaptureMove(Move move, Piece capture)
	{
		this(move.piece, move.to, move.from, capture);
	}

	public CaptureMove(Piece piece, Position to, Position from, Piece capture) {
		super(piece, to, from);
		this.capture = capture;
	}
	
	@Override
	public void undoMove() {
		//restore capture
		Board board = Board.getInstance();
		Piece restored = board.createPiece(capture.getValue(), to);
		board.pieces.add(restored);
		
		//move the piece back
		super.undoMove();
	}
	
	public Piece getCapture() {
		return capture;
	}
	
	@Override
	public String toString() {
		return super.toString() + " - (" + this.capture.toString() + ") was captured";
	}

}
