package com.wishbone.enums;

import com.wishbone.MoveGenerator;

/**
 * Used to determine the result of an {@link MoveGenerator.move}
 * 
 * @author Ross
 */
public enum Result {

	OK,
	CAPTURE,
	ENPASSANT_CAPTURE,
	ENPASSANT,
	CASTLING,
	PROMOTION,
	
	FAIL,
	
	IN_CHECK,
	OPPONENT_IN_CHECK,
	CHECKMATE,
	STALEMATE,
}
