package com.wishbone;

import com.wishbone.enums.Result;
import com.wishbone.moves.Move;

/**
 * The main class for the engine, {@link BoardPanel} will call this class for access
 * to board state and move searching.
 * 
 * @author Ross
 */
public class Engine {

	/**
	 * Used to instantiate a new game (if no FEN string is provided)
	 */
	public static final String NEW_GAME_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
	
	private static Engine instance;
	
	public static Engine getInstance(boolean fresh) {
		if (instance != null || fresh) {
			instance = new Engine();
		}
		
		return instance;
	}
	
	public MoveGenerator gen;
	public Search search;
	
	public static boolean stopped;
	
	//How deep the AI will try and search
	private int searchDepth = 10;
	
	//Search limit in seconds
	private int thinkingTime = 20;
	
	public void position(String fen) {
		gen = new MoveGenerator(fen);
	}

	/**
	 * Performs a move using the piece at board index from,
	 * moving it to the board index at index to. The move will
	 * return a State object that shows how the move proceeded,
	 * State.FAIL means that no move was made.
	 * 
	 * @return {@link Move} - A value representing the result of the move
	 */
	public Result move(int from, int to) {
		Result result = gen.move(from, to);
		
		if (result == Result.IN_CHECK) {
			gen.undo_move();
		}

		System.out.println(result);
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Takes a Move object instead of board indexes.
	 */
	public Result move(Move move) {
		return move(move.getFrom(), move.getTo());
	}

	/**
	 * Asks the engine to search for a move, note that
	 * the move is not made on the internal board.
	 * 
	 * @param colour - the colour of the side to search
	 * @return the recommended move to be made
	 */
	public Move search(int colour) {
		
		if (search == null) {
			search = new Search(gen);
		}
		
		return search.search(colour, searchDepth, thinkingTime);
	}

	/**
	 * Fully undoes the last move played.
	 * 
	 */
	public void undoLastMove() {
		gen.undo_move();
	}

	public boolean isWhiteMove() {
		return gen.board.isWhiteToMove();
	}

	public int getSearchDepth() {
		return searchDepth;
	}

	public void setSearchDepth(int searchDepth) {
		this.searchDepth = searchDepth;
	}

	public int getThinkingTime() {
		return thinkingTime;
	}

	public void setThinkingTime(int thinkingTime) {
		this.thinkingTime = thinkingTime;
	}

}
