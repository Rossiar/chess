package com.wishbone;

import java.util.ArrayList;

import com.wishbone.enums.Position;
import com.wishbone.pieces.Bishop;
import com.wishbone.pieces.BlackKing;
import com.wishbone.pieces.BlackPawn;
import com.wishbone.pieces.Knight;
import com.wishbone.pieces.Piece;
import com.wishbone.pieces.Queen;
import com.wishbone.pieces.Rook;
import com.wishbone.pieces.WhiteKing;
import com.wishbone.pieces.WhitePawn;

/**
 * Holds the internal board representation and state, also responsible
 * for writing state out to String.
 * 
 * @author Ross
 */
public class Board {

	/**
	 * Long values that represent bitboards that are occupied along each rank 
	 * and file individually.
	 */
	public static final long RANK_1 = 0x00_00_00_00_00_00_00_FFL;
	public static final long RANK_2 = 0x00_00_00_00_00_00_FF_00L;
	public static final long RANK_3 = 0x00_00_00_00_00_FF_00_00L;
	public static final long RANK_4 = 0x00_00_00_00_FF_00_00_00L;
	public static final long RANK_5 = 0x00_00_00_FF_00_00_00_00L;
	public static final long RANK_6 = 0x00_00_FF_00_00_00_00_00L;
	public static final long RANK_7 = 0x00_FF_00_00_00_00_00_00L;
	public static final long RANK_8 = 0xFF_00_00_00_00_00_00_00L;
	public static final long FILE_H = 0x0101010101010101L;
	public static final long FILE_G = 0x202020202020202L;
	public static final long FILE_F = 0x404040404040404L;
	public static final long FILE_E = 0x808080808080808L;
	public static final long FILE_D = 0x1010101010101010L;
	public static final long FILE_C = 0x2020202020202020L;
	public static final long FILE_B = 0x4040404040404040L;
	public static final long FILE_A = 0x8080808080808080L;

	/**
	 * Used for turning board indexes into Standard Algebraic Notation (SAN)
	 */
	public static final String[] ALBEBRA_FILES = {"h", "g", "f", "e", "d", 
		"c", "b", "a"};

	private static Board instance;
	
	/*
	 * The current ability to castle, represented as a bitboard
	 * with the squares that are clear to move to represented as 1s.
	 */
	private long castlingMoves = 0L;

	/*
	 * Whose turn it is to move.
	 */
	private boolean whiteToMove;

	/*
	 * The current enpassant target square, represented as a
	 * board index. (or -1 if none exists).
	 */
	private Position enpassantTarget = Position.ILLEGAL;

	/*
	 * The halfmove clock.
	 */
	private int halfmoves = 0;

	/*
	 * The last value that the halfmove clock held.
	 */
	private int oldHalfmoves = 0;

	/*
	 * The move clock.
	 */
	private int moves = 1;

	public ArrayList<Piece> pieces = new ArrayList<Piece>();


	public static Board getInstance() {
		if (instance == null)
		{
			instance = new Board(Engine.NEW_GAME_FEN);
		}
		
		return instance;
	}

	public static Board getInstance(String fen) {
		instance = new Board(fen);
		return instance;
	}

	public Piece getPiece(Position pos) {
		for (Piece p : pieces) {
			if (p.getPos() == pos) {
				return p;
			}
		}

		return null;
	}

	public Piece getPieceByCode(int code) {
		for (Piece p : pieces) {
			if (p.getValue() == code) {
				return p;
			}
		}

		return null;
	}

	public long getOccupied() {
		long occupied = 0L;

		for (Piece piece : pieces) {
			occupied |= piece.getBoard();
		}

		return occupied;
	}

	public void printOccupied() {
		printBoard(getOccupied());
	}

	public long getPiecesOfColour(int colour) {
		long occupied = 0L;

		for (Piece piece : pieces) {
			if (piece.getColour() == colour) {
				occupied |= piece.getBoard();
			}
		}

		return occupied;
	}
	/**
	 * Constructs a board that represents the board state,
	 * given as a FEN string.
	 */
	private Board(String fen) {
		String[] args = fen.split("\\s");

		//create the bitboards
		createBoards(args[0]);

		//extract who is to move
		whiteToMove = args[1].equals("w");

		//extract castling rights
		String castling = args[2];
		if (castling.contains("K"))	castlingMoves |= (1L << 1);
		if (castling.contains("Q"))	castlingMoves |= (1L << 5);
		if (castling.contains("k"))	castlingMoves |= (1L << 57);
		if (castling.contains("q"))	castlingMoves |= (1L << 61);

		//extract enpassant target (if any exists)
		if (!args[3].equals("-")) setEnpassantTarget(Position.valueOf(args[3].toUpperCase()));

		//extract halfmove clock
		halfmoves = Integer.parseInt(args[4]);

		//extract move clock
		moves = Integer.parseInt(args[5]);
	}

	/*
	 * Creates the bitboards from the given FEN string
	 */
	private void createBoards(String f) {
		char[] fen = f.toCharArray();
		int rank = 7, file = 7;

		for (char ch : fen) {
			if (ch == '/') {
				rank--;
				file = 7;
				continue;
			} else if (Character.isDigit(ch)) {
				file -= Character.getNumericValue(ch);
			} else {
				int square = (rank * 8) + file;
				Piece piece = createPiece(ch, Position.valueOf(square));

				//add the piece to the board
				if (piece != null) {
					pieces.add(piece);
				}

				file--;
			}
		}
	}

	public Piece createPiece(char ch, Position startpos) {

		switch (ch) {
		case 'P': return new WhitePawn(startpos);
		case 'p': return new BlackPawn(startpos);
		case 'N': return new Knight(0, startpos);
		case 'n': return new Knight(1, startpos);
		case 'B': return new Bishop(0, startpos);
		case 'b': return new Bishop(1, startpos);
		case 'R': return new Rook(0, startpos);
		case 'r': return new Rook(1, startpos);
		case 'Q': return new Queen(0, startpos);
		case 'q': return new Queen(1, startpos);
		case 'K': return new WhiteKing(startpos);
		case 'k': return new BlackKing(startpos);
		default: return null;
		}
	}

	public Piece createPiece(int i, Position startpos) {

		switch (i) {
		case 2: return new WhitePawn(startpos);
		case 3: return new BlackPawn(startpos);
		case 4: return new Knight(0, startpos);
		case 5: return new Knight(1, startpos);
		case 6: return new Bishop(0, startpos);
		case 7: return new Bishop(1, startpos);
		case 8: return new Rook(0, startpos);
		case 9: return new Rook(1, startpos);
		case 10: return new Queen(0, startpos);
		case 11: return new Queen(1, startpos);
		case 12: return new WhiteKing(startpos);
		case 13: return new BlackKing(startpos);
		default: return null;
		}
	}

	/**
	 * Generates all attacks for the given colour
	 * on the current board.
	 */
	public long getAttacks(int colour) {
		long attacks = 0L;

		for (Piece piece : pieces) {
			if (piece.isColour(colour)) {
				attacks |= piece.generateAttacks();
			}
		}

		return attacks;
	}

	/**
	 * Used to determine if a given board contains any pieces.
	 */
	public static boolean isEmpty(long board) {
		return Long.bitCount(board) == 0;
	}

	/**
	 * Prints the bitboard to the console as an 8x8 board of
	 * 1s and 0s.
	 */
	public static void printBoard(long board) {
		String out = "";
		char[] chars = getBoardAsBinaryString(board).toCharArray();

		for (int i=0; i < chars.length; i++) {
			char ch = chars[i];

			if (i % 8 == 0) {
				out += "\n";
			}

			out += ch;
		}

		System.out.println(out+"\n");
	}

	/**
	 * Returns the board as a string of 1s and 0s.
	 */
	public static String getBoardAsBinaryString(long board) {
		String s = Long.toBinaryString(board);
		int zeroes = 64 - s.length();
		String bs = "";

		if (zeroes >= 0) {
			for (int i = 0; i < zeroes; i++) {
				bs += 0;
			}

			return bs += s;
		}

		return s;
	}


	/**
	 * Takes a board index and returns the Standard
	 * Algebraic Notation (SAN) representation of
	 * that index.
	 */
	public static String indexToAlgebra(int index) {
		if (index < 0)
			return "invalid";

		String algebra = "";

		algebra += ALBEBRA_FILES[index % 8];
		algebra += (index / 8)+1;

		return algebra;
	}

	/**
	 * Takes a Standard Algebraic Notation (SAN)
	 * representation of a board square and
	 * returns it as a board index.
	 */
	public static int algebraToIndex(String exp) {
		int file = 0;
		for (int i = 0; i < ALBEBRA_FILES.length; i++) {
			if (ALBEBRA_FILES[i].equals(exp.substring(0, 1).toLowerCase())) {
				file = i;
				break;
			}
		}

		int rank = Integer.parseInt(exp.substring(1))-1;

		return (file+(rank*8));
	}

	public void nextMove() {
		whiteToMove = !whiteToMove;
	}

	public void incrementMoves() {
		moves++;
	}

	public void decrementMoves() {
		moves--;
	}

	public void resetEnPassantTarget() {
		setEnpassantTarget(Position.ILLEGAL);
	}

	public void incrementHalfMoves() {
		halfmoves++;
	}

	public void resetHalfMoves() {
		oldHalfmoves = halfmoves;
		halfmoves = 0;
	}

	public void restoreHalfMoves() {
		halfmoves = oldHalfmoves;
	}

	// GETTERS AND SETTERS //

	public long getCastlingMoves() {
		return castlingMoves;
	}

	public boolean checkCastlingMoves(String moves) {
		String rights = "";
		
		if (isEmpty(castlingMoves & (1L << 1))) rights += "K";
		if (isEmpty(castlingMoves & (1L << 5))) rights += "Q";
		if (isEmpty(castlingMoves & (1L << 57))) rights += "k";
		if (isEmpty(castlingMoves & (1L << 61))) rights += "q";
		if (rights.equals("")) rights = "-";
		
		return rights.contains(moves);
	}

	public void setCastlingMoves(long castlingMoves) {
		this.castlingMoves = castlingMoves;
	}

	public boolean isWhiteToMove() {
		return whiteToMove;
	}

	public void setWhiteToMove(boolean whiteToMove) {
		this.whiteToMove = whiteToMove;
	}

	public int getHalfmoves() {
		return halfmoves;
	}

	public void setHalfmoves(int halfmoves) {
		this.halfmoves = halfmoves;
	}

	public int getOldHalfmoves() {
		return oldHalfmoves;
	}

	public void setOldHalfmoves(int oldHalfmoves) {
		this.oldHalfmoves = oldHalfmoves;
	}

	public int getMoves() {
		return moves;
	}

	public void setMoves(int moves) {
		this.moves = moves;
	}

	public Position getEnpassantTarget() {
		return enpassantTarget;
	}

	public void setEnpassantTarget(Position enpassantTarget) {
		this.enpassantTarget = enpassantTarget;
	}
}
