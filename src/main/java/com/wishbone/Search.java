package com.wishbone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.wishbone.enums.Result;
import com.wishbone.moves.Move;
import com.wishbone.nodes.MaxNode;
import com.wishbone.nodes.MinNode;
import com.wishbone.nodes.Node;
import com.wishbone.pieces.Piece;

/**
 * Responsible for searching ahead and selecting
 * the best move, uses the board representation
 * provided by Board and the move generating
 * and making capabilities of MoveGenerator.
 * 
 * @author Ross
 */
public class Search {

	private MoveGenerator gen;

	private int fails = 0;
	private int cutoffs = 0;
	private int maximisingPlayer = 1;
	private int thinkingTime;
	private long start;
	private int depth;
	private Move bestmove = null;

	//TODO: Remove.
	static String bug = "rn2kb1r/pp2p1pp/2p5/4NP2/5B2/8/PPPQ1P1P/RN2KB1R b KQkq - 0 11";
	static String middlegame = "r2q3k/pn2bprp/4pNp1/2p1PbQ1/3p1P2/5NR1/PPP3PP/2B2RK1 b - - 0 25";
	static String foolsmate = "rnbqkbnr/pppp1ppp/8/4p3/6P1/5P2/PPPPP2P/RNBQKBNR b KQkq - 0 1";
	static String obvious = "rnbqkbnr/ppppppp1/8/7p/6P2/8/PPPPPP1P/RNBQKBNR w KQkq - 0 1";

	public Search(MoveGenerator gen) {
		this.gen = gen;
	}

	public static void main(String[] args) {
		Engine eng = Engine.getInstance(true);
		eng.position(bug);
		Search search = new Search(eng.gen);

		long start = System.nanoTime();
		Move chosen = search.search(1, 4, eng.getThinkingTime());
		long end = System.nanoTime();

		System.out.println("bestmove " + chosen);

		System.out.println(Node.nodes+" nodes");
		System.out.println(search.fails+" failures");
		System.out.println(search.cutoffs+" cutoffs");

		long seconds = TimeUnit.NANOSECONDS.toMillis(end - start);
		System.out.println("Search took " + seconds + "ms");
	}


	/**
	 * Searches for the bestmove on the current board
	 * state.
	 * 
	 * @param colour - player whose moves to be searched for
	 * @param depth - how far down the tree to go
	 * @param time - how long in seconds to spend searching
	 * @return the bestmove found within the time
	 */
	public Move search(int colour, int depth, long time) {

		//instantiate root node at depth 0
		Node origin = new MaxNode(0);
		this.depth = depth;

		double best = Double.NEGATIVE_INFINITY;

		alphabeta(origin, 0, Double.NEGATIVE_INFINITY,
				Double.POSITIVE_INFINITY);

		List<Move> keys = new ArrayList<Move>(origin.getChildren().keySet());
		Collections.sort(keys, Collections.reverseOrder());

		for (Move move : keys) {
			Node child = origin.getChild(move);

			System.out.println(move.toString() + " " + move.getScore() + " -> " + child.toString());

			double score = child.getValue();
			if (score > best) {
				best = score;
				bestmove = move;
			}
		}

		/*		//deepen...iteratively
		for (int i = 1; i < depth; i++) {
			alphabeta(origin, i, Double.NEGATIVE_INFINITY,
					Double.POSITIVE_INFINITY, colour);

			for (Entry<Move, Node> entry : origin.getChildren().entrySet()) {
				Node child = entry.getValue();
				Move move = entry.getKey();

				double score = child.getValue();
				if (score > best) {
					best = score;
					bestmove = move;
				}
			}
		}*/

		return bestmove;
	}

	/**
	 * Used to execute the alpha-beta algorithm and also construct a game
	 * tree to find the best node. Performs cutoffs if b <= a.
	 * 
	 * @param node - the current node being examined for children
	 * @param depth - counter so the function knows when to stop
	 * @param a - is the minimum value that the MAX player can be assured of (CPU)
	 * @param b - is the maximum value that the MIN player can be assured of
	 * @return score of node
	 */
	public double alphabeta(Node node, int depth, double a, double b) {
		int colour = node instanceof MaxNode ? 1 : 0;

		ArrayList<Move> moves = gen.getMoves(colour);

		depth++;

		//Iterate through all available moves
		for (Move move : moves) {

			//prune tree here as b <= a, so maximising player will never come here
			if (b <= a) {
				cutoffs++;
				break;
			}

			//make the move on the internal board
			Result result = gen.move(move);

			//if the move failed, prune the tree
			if (result == Result.FAIL) {
				fails++;
				continue;
			}

			//ignore making moves that would put us in check
			if (result == Result.IN_CHECK) {
				gen.undo_move();
				continue;
			}

			Node child = null;
			
			if (depth == this.depth) {
				double score = evaluate(); 
				gen.undo_move();
				return score;
			}

			if (node instanceof MaxNode) {
				child = new MinNode(depth);
				a = Math.max(a, alphabeta(child, depth, a, b));

			} else {
				child = new MaxNode(depth);
				b = Math.min(b, alphabeta(child, depth, a, b));
			}

			node.addChild(move, child);
			gen.undo_move();
		}

		if (node instanceof MaxNode) {
			node.setValue(a);
			return a;
		} else {
			node.setValue(b);
			return b;
		}
	}
	
	/**
	 * Claude Shannon Symmetric Evaluation Function (1949):
	 * f(p) = 200(K-K') + 9(Q-Q') + 5(R-R') + 3(B-B' + N-N') + 1(P-P')
	 * - 0.5(D-D' + S-S' + I-I')
	 * + 0.1(M-M') + ...
	 *
	 * KQRBNP = number of kings, queens, rooks, bishops, knights and pawns
	 * D,S,I = doubled, blocked and isolated pawns
	 * M = Mobility (the number of legal moves)
	 * 
	 * We are disregarding D, S, I and M for now
	 * @param colour - the colour to evaluate the board by
	 * @return the heuristic value of this board
	 */
	public double evaluate() {
		double score = 0;
		int colour = 1;
		int opponentColour = 0;

		if (gen.isKingCheck(colour)) {
			score -= 200;
		}

		if (gen.isKingCheckmate(colour)) {
			score -= 1000;
		}

		if (gen.isKingCheck(opponentColour)) {
			score += 100;
		}

		if (gen.isKingCheckmate(opponentColour)) {
			score += 800;
		}
		
		for (Piece piece : gen.board.pieces) {
			if (piece.isColour(colour)) {
				score += piece.getValue();
			} else {
				score -= piece.getValue();
			}
		}

		return score;
	}

}
