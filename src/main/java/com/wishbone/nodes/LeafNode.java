package com.wishbone.nodes;

public class LeafNode extends Node {
	
	public static int leaves = 0;

	public LeafNode(int depth, double value) {
		super(depth);
		this.value = value;
		leaves++;
	}
	
	@Override
	public String toString() {
		return "Leaf node at depth " + depth + " with value " + value;
	}

}
