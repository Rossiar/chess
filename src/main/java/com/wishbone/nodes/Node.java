package com.wishbone.nodes;

import java.util.HashMap;
import java.util.Map.Entry;

import com.wishbone.Search;
import com.wishbone.moves.Move;

/**
 * Used by {@link Search} to form the game tree that
 * it is searching in.
 * 
 * @author Ross
 */
public class Node {

	/*
	 * The children of the node, represented as
	 * the child nodes and the moves that are used
	 * to reach them.
	 */
	protected HashMap<Move, Node> children = 
			new HashMap<Move, Node>();
	
	//The depth of this node in the game tree
	protected int depth;
	
	//The heuristic value of this node
	protected double value;

	//Statistic for the total number of nodes
	public static int nodes = 0;
	
	/*
	 * Returns a string representation of the node
	 * and all of its children, do not use this
	 * on large nodes.
	 */
	public static String printFull(Node node) {
		String string = "";
		String offset = new String(new char[node.depth]).replace("\0", " ");
		
		string += node.toString() + "\n";
		
		for (Entry<Move, Node> entry : node.children.entrySet()) {
			Node child = entry.getValue();
			Move move = entry.getKey();
			
			string += offset + move.toString() + " -> " + printFull(child);
			
		}
		
		return string;
	}
	
	protected Node(int depth) {
		this.depth = depth;
		nodes++;
	}
	
	public void addChild(Move move, Node child) {
		children.put(move, child);
	}
	
	public Node getChild(Move move) {
		return children.get(move);
	}
	
	public boolean hasChildren() {
		return children.size() > 1;
	}
	
	@Override
	public String toString() {
		return "Node at depth " + depth + " with value " + value;
	}
	
	public HashMap<Move, Node> getChildren() {
		return children;
	}
	
	public int getDepth() {
		return depth;
	}

	public double getValue() {
		return value;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
}
