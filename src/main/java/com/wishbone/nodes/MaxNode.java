package com.wishbone.nodes;

public class MaxNode extends Node {

	public MaxNode(int depth) {
		super(depth);
	}
	
	@Override
	public String toString() {
		return "Max node at depth " + depth + " with value " + value;
	}

}
