package com.wishbone.nodes;

public class MinNode extends Node {

	public MinNode(int depth) {
		super(depth);
	}
	
	@Override
	public String toString() {
		return "Min node at depth " + depth + " with value " + value;
	}

}
