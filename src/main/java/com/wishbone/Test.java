package com.wishbone;

import com.wishbone.enums.Position;
import com.wishbone.pieces.WhitePawn;

public class Test {
	
	public static void main(String args[]) {
		long tmp = (1L << 18);
		
		Board.printBoard(tmp);
		tmp <<= 7;
		Board.printBoard(tmp);
		tmp <<= 7;
		Board.printBoard(tmp);
		tmp <<= 7;
		Board.printBoard(tmp);
		System.out.println(Board.isEmpty(tmp & Board.FILE_A));
		
		
		new MoveGenerator(Engine.NEW_GAME_FEN);
		WhitePawn wp = new WhitePawn(Position.E4);
		
		Board.getInstance().printOccupied();
		
		Board.printBoard(wp.getBoard());
		Board.printBoard(wp.generate());
		
		//System.out.println(testPiece.move(Board.algebraToIndex("g5")));
	}

}
