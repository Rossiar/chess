package com.wishbone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import com.wishbone.enums.Position;
import com.wishbone.enums.Result;
import com.wishbone.moves.CaptureMove;
import com.wishbone.moves.EnpassantMove;
import com.wishbone.moves.Move;
import com.wishbone.pieces.BlackKing;
import com.wishbone.pieces.Pawn;
import com.wishbone.pieces.Piece;
import com.wishbone.pieces.Rook;
import com.wishbone.pieces.WhiteKing;

/**
 * Responsible for generating all moves for all pieces
 * and for making and unmaking moves.
 * 
 * @author Ross
 */
public class MoveGenerator {

	public Board board;

	/**
	 * The list of moves that have been made on the board.
	 */
	private LinkedList<Move> moves = new LinkedList<Move>();

	public MoveGenerator(String fen) {
		board = Board.getInstance(fen);
	}

	/**
	 * Executes the given move
	 * 
	 * @param move - the move to execute
	 * @return The state of the move that was made, or State.FAIL if
	 * no move was made. 
	 */
	public Result move(Move move) {
		return move(move.getFrom(), move.getTo());
	}

	public Result move(String from, String to) {
		return move(Board.algebraToIndex(from), Board.algebraToIndex(to));
	}

	/**
	 * Moves the piece located at board index from and performs a move
	 * ending at board index to, updates any relevant board variables
	 * and records the move.
	 * 
	 * @param from - square we moved from
	 * @param to - square we are moving to
	 * @return The state of the move that was made, or State.FAIL if
	 * no move was made. 
	 */
	public Result move(int from, int to) {
		
		Result state = Result.OK;
		Piece piece = board.getPiece(Position.valueOf(from));
		Move move = piece.move(Position.valueOf(to));
		
		if (move == null) {
			return Result.FAIL;
		}

		//add the move to our list
		moves.add(move);
		
		//reset the enpassant target if the move was not an enpassant
		if (!(move instanceof EnpassantMove)) {
			board.resetEnPassantTarget();
		}

		//increment the full move counter on every black move
		if (piece.isColour(1)) {
			board.incrementMoves();
		}

		//increment the halfmove clock if no pawn move or capture
		if (move instanceof CaptureMove || piece instanceof Pawn) {
			board.resetHalfMoves();
		} else {
			board.incrementHalfMoves();
		}

		//update turn
		board.nextMove();

		//delete castling rights if pieces move
		if (piece instanceof Rook) {
			//calculate the offset to the kings target square and remove
			int offset = from % 2 == 0 ? 1 : -2;
			board.setCastlingMoves(board.getCastlingMoves() & ~(1L << from+offset));
		} else if (piece instanceof WhiteKing) {
			board.setCastlingMoves(board.getCastlingMoves() & ~Board.RANK_1);
		} else if (piece instanceof BlackKing) { 
			board.setCastlingMoves(board.getCastlingMoves() & ~Board.RANK_8);
		}
		
		int colour = piece.getColour();
		int opponentColour = colour ^ 1;

		if (isKingCheck(opponentColour)) {

			//process possible checkmate
			if (isKingCheckmate(opponentColour)) {
				return Result.CHECKMATE;
			}

			//the opponent king is in check
			return Result.OPPONENT_IN_CHECK;

		} else {
			//process possible stalemate
			if (getMoves(opponentColour).size() < 1) {
				return Result.STALEMATE;
			}
		}

		if (isKingCheck(colour)) {
			return Result.IN_CHECK;
		}

		return state;
	}

	/**
	 * Special move generator only used when we are exploring moves
	 * that break us out of check.
	 */
	public Result checkmateMove(int from, int to) {
		
		Result state = Result.OK;
		Piece piece = board.getPiece(Position.valueOf(from));
		Move move = piece.move(Position.valueOf(to));
		
		if (move == null) {
			return Result.FAIL;
		}

		//add the move to our list
		moves.add(move);
		
		//reset the enpassant target if the move was not an enpassant
		if (!(move instanceof EnpassantMove)) {
			board.resetEnPassantTarget();
		}

		//increment the full move counter on every black move
		if (piece.isColour(1)) {
			board.incrementMoves();
		}

		//increment the halfmove clock if no pawn move or capture
		if (move instanceof CaptureMove || piece instanceof Pawn) {
			board.resetHalfMoves();
		} else {
			board.incrementHalfMoves();
		}

		//update turn
		board.nextMove();

		//delete castling rights if pieces move
		if (piece instanceof Rook) {
			//calculate the offset to the kings target square and remove
			int offset = from % 2 == 0 ? 1 : -2;
			board.setCastlingMoves(board.getCastlingMoves() & ~(1L << from+offset));
		} else if (piece instanceof WhiteKing) {
			board.setCastlingMoves(board.getCastlingMoves() & ~Board.RANK_1);
		} else if (piece instanceof BlackKing) { 
			board.setCastlingMoves(board.getCastlingMoves() & ~Board.RANK_8);
		}
		
		int colour = piece.getColour();
		int opponentColour = colour ^ 1;

		if (isKingCheck(opponentColour)) {
			//the opponent king is in check
			return Result.OPPONENT_IN_CHECK;

		} else {
			//process possible stalemate
			if (getMoves(opponentColour).size() < 1) {
				return Result.STALEMATE;
			}
		}

		if (isKingCheck(colour)) {
			return Result.IN_CHECK;
		}

		return state;
	}

	/**
	 * Will take the last move from the list and fully undo it.
	 */
	public void undo_move() {
		Move last = moves.pollLast();
		
		if (last != null) {
			last.undoMove();
		} else {
			System.err.println("No move to undo!");
		}
	}

	/**
	 * Will get all the moves available for the given colour.
	 * 
	 * @param colour - the colour to generate moves for
	 * @return a list of moves the player can make
	 */
	public ArrayList<Move> getMoves(int colour) {
		ArrayList<Move> moves = new ArrayList<Move>();
		for (Piece piece : board.pieces) {
			if (piece.isColour(colour)) {
				Collections.addAll(moves, piece.getMoves());
			}
		}

		Collections.sort(moves, Collections.reverseOrder());
		return moves;
	}

	/**
	 * Checks if the king of a given side is in
	 * check on the current board.
	 */
	public boolean isKingCheck(int colour) {
		//mask captures to only be valid when an opponent's piece is present
		return Board.isEmpty(board.getAttacks(colour ^ 1) & 
				board.getPieceByCode(12 + colour).getBoard());
	}

	/**
	 * Checks if the king of a given side has
	 * been checkmated on the current board.
	 */
	public boolean isKingCheckmate(int colour) {

		//look through all possible moves that we can make
		for (Move move : getMoves(colour)) {
			//if any are OK then we aren't in check
			Result r = checkmateMove(move.getFrom(), move.getTo());
			System.out.println(r);
			if (r == Result.OK) {
				return false;
			}
			
		}

		return true;
	}
	
	public long generate(int position) {
		return board.getPiece(Position.valueOf(position)).generate();
	}
	
	public long generate(String position) {
		return generate(Board.algebraToIndex(position));
	}

	/**
	 * Generates all possible moves for the given colour
	 * on the current board.
	 */
	public long generateAllMoves(int colour) {
		long attacks = 0L;

		for (Piece piece : board.pieces) {
			if (piece.isColour(colour)) {
				attacks |= piece.generate();
			}
		}

		return attacks;
	}

	public LinkedList<Move> getMoveList() {
		return moves;
	}

}
