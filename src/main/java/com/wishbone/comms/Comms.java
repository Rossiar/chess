package com.wishbone.comms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import com.wishbone.Board;
import com.wishbone.Engine;
import com.wishbone.moves.Move;

/**
 * Redundant class used for communicating with
 * GUIs using the UCI protocol
 * 
 * @author Ross
 */
public class Comms implements Runnable {
	
	public static final String NEW_LINE = String.format("%n");
	public static final String FEN_REGEX = 
			"(\\w+?/){7}\\w+\\s(w|b)\\s\\S+\\s\\S+\\s\\d+\\s\\d+";

	public LinkedBlockingQueue<String> inQueue;
	public LinkedBlockingQueue<String> outQueue;
	public InputStream in;
	public OutputStream out;
	private ExecutorService service;
	private Engine engine;
	private boolean running;

	public static void main(String[] args) {
		new Comms();
	}

	public Comms() {
		engine = Engine.getInstance(true);
		inQueue = new LinkedBlockingQueue<String>();
		outQueue = new LinkedBlockingQueue<String>();
		service = Executors.newFixedThreadPool(3);
		new Thread(this).start();
	}
	
	public synchronized boolean isRunning() {
		return running;
	}

	@Override
	public synchronized void run() {
		running = true;
		service.execute(new InputHandler());
		service.execute(new OutputHandler());
		service.execute(new ProcessHandler());
	}

	public synchronized void stop() {
		this.running = false;
		service.shutdownNow();
	}
	
	/*
	 * Process the instructions being received on ProcessHandler.
	 */
	public String process(String input) {
		switch(input) {
		case "uci":
			return "id name Unknown" + NEW_LINE + "id author Ross Henderson" +
			NEW_LINE + "uciok";
		case "isready":
			return "readyok";
		case "ucinewgame":
			engine = Engine.getInstance(true);
			return "";
		case "stop":
			Engine.stopped = true;
			return "";
		case "quit":
			stop();
			System.exit(0);
			return "";
		}
		
		if (input.startsWith("position")) {
			String fen = Engine.NEW_GAME_FEN;
			
			if (!input.contains("startpos")) {
				fen = input.substring(8).trim();
			}
			
			//start a new board with the position
			engine.position(fen);
			return "";
		}
		
		if (input.startsWith("go")) {
			//TODO: deal with this later
			//String commands = input.substring(2).trim();
			
			Move bestMove = engine.search(1);
			String to = Board.indexToAlgebra(bestMove.getTo());
			String from = Board.indexToAlgebra(bestMove.getFrom());
			return "bestmove " + from + to;
		}
		
		System.err.println("Unkown input");
		return "";
	}

	/**
	 * Handle items waiting in the queue and process them, placing our
	 * output in the list of items waiting to be sent to the console.
	 * 
	 * @author Ross
	 */
	private class ProcessHandler implements Runnable {
		@Override
		public void run() {
			while (isRunning()) {
				String input = null;

				try {
					input = inQueue.take();
					String response = process(input.trim());

					if (response != null) {
						outQueue.add(response);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
					running = false;
				}
			}
		}

	}

	/**
	 * Take input from the console and place in our waiting queue
	 * @author ross
	 *
	 */
	private class InputHandler implements Runnable {
		@Override
		public void run() {
			BufferedReader consoleIn = new BufferedReader(
					new InputStreamReader(System.in));
			while (isRunning()) {
				String input = null;
				
				try {
					input = consoleIn.readLine();

					if (input != null && input.length() > 0) {
						inQueue.add(input);
					}
					
				} catch (IOException e) {
					e.printStackTrace();
					running = false;
				}
			}
		}

	}

	/**
	 * Output messages we generate to the console.
	 * @author ross
	 *
	 */
	private class OutputHandler implements Runnable {
		@Override
		public void run() {
			PrintWriter consoleOut = new PrintWriter(System.out);
			while (isRunning()) {
				try {
					String out = outQueue.take();
					if (out != null && out.length() > 0) {
						consoleOut.println(out);
						consoleOut.flush();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
					running = false;
				}
			}
		}

	}
}
