package com.wishbone.pieces;

import com.wishbone.enums.Position;


public class Queen extends SlidingPiece {

	public Queen(int colour, Position startpos) {
		super(colour, startpos);
		value = 10 + colour;
	}

	@Override
	public long generate() {
		return generateDiagonalRays() | generateRays();
	}
}
