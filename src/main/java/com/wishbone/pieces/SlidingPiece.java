package com.wishbone.pieces;

import com.wishbone.Board;
import com.wishbone.enums.Position;

public abstract class SlidingPiece extends Piece {

	public SlidingPiece(int colour, Position position) {
		super(colour, position);
	}

	@Override
	public long generateAttacks() {
		return generate();
	}

	protected long generateDiagonalRays() {

		long occupied = Board.getInstance().getOccupied();
		long attacks = 0L;

		//upper left
		long tmp = bitboard << 9;
		while (Board.isEmpty(tmp & Board.FILE_H) && !Board.isEmpty(tmp)) {
			if (!Board.isEmpty(occupied & tmp)) {
				attacks |= tmp;
				break;
			}

			attacks |= tmp;
			tmp <<= 9;
		}

		//lower left
		tmp = bitboard >>> 7;
			while (Board.isEmpty(tmp & Board.FILE_H) && !Board.isEmpty(tmp)) {
				if (!Board.isEmpty(occupied & tmp)) {
					attacks |= tmp;
					break;
				}

				attacks |= tmp;
				tmp >>= 7;
			}

			//upper right
			tmp = bitboard << 7;
			while (Board.isEmpty(tmp & Board.FILE_A) && !Board.isEmpty(tmp)) {
				if (!Board.isEmpty(occupied & tmp)) {
					attacks |= tmp;
					break;
				}

				attacks |= tmp;
				tmp <<= 7;
			}

			//lower right
			tmp = bitboard >>> 9;
				while (Board.isEmpty(tmp & Board.FILE_A) && !Board.isEmpty(tmp)) {
					if (!Board.isEmpty(occupied & tmp)) {
						attacks |= tmp;
						break;
					}

					attacks |= tmp;
					tmp >>>= 9;
				}

				return attacks & ~Board.getInstance().getPiecesOfColour(colour);
	}

	protected long generateRays() {

		long occupied = Board.getInstance().getOccupied();
		long attacks = 0L;

		//search up the file from our square
		long tmp = bitboard << 8;
		while (!Board.isEmpty(tmp)) {
			if (!Board.isEmpty(occupied & tmp)) {
				attacks |= tmp;
				break;
			}

			attacks |= tmp;
			tmp <<= 8;
		}

		//search down the file from our square
		tmp = bitboard >>> 8;
			while (!Board.isEmpty(tmp)) {
				if (!Board.isEmpty(occupied & tmp)) {
					attacks |= tmp;
					break;
				}

				attacks |= tmp;
				tmp >>>= 8;
			}

			//search left along the rank from our square
			tmp = bitboard << 1;
			while (Board.isEmpty(tmp & Board.FILE_H) && !Board.isEmpty(tmp)) {
				if (!Board.isEmpty(occupied & tmp)) {
					attacks |= tmp;
					break;
				}

				attacks |= tmp;
				tmp <<= 1;
			}


			//search right along the rank from our square
			tmp = bitboard >>> 1;
				while (Board.isEmpty(tmp & Board.FILE_A) && !Board.isEmpty(tmp)) {
					if (!Board.isEmpty(occupied & tmp)) {
						attacks |= tmp;
						break;
					}

					attacks |= tmp;
					tmp >>>= 1;
				}

				return attacks & ~Board.getInstance().getPiecesOfColour(colour);
	}

}
