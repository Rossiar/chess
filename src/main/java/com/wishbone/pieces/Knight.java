package com.wishbone.pieces;

import com.wishbone.Board;
import com.wishbone.enums.Position;

public class Knight extends Piece {

	public Knight(int colour, Position startpos) {
		super(colour, startpos);
		value = 4 + colour;
	}

	@Override
	public long generate() {
		long moves = 0L;

		//up
		moves |= bitboard << 15 & ~Board.FILE_A;
		moves |= bitboard << 17 & ~Board.FILE_H;

		//down
		moves |= bitboard >> 15 & ~Board.FILE_H;
		moves |= bitboard >> 17 & ~Board.FILE_A;        

		//left
		moves |= (bitboard >> 6 & ~Board.FILE_G & ~Board.FILE_H);
		moves |= bitboard << 10 & ~Board.FILE_G & ~Board.FILE_H;

		//right
		moves |= bitboard >> 10 & ~Board.FILE_A & ~Board.FILE_B;
		moves |= bitboard << 6 & ~Board.FILE_A & ~Board.FILE_B;

		//make sure we can't move/capture our own pieces
		moves &= ~Board.getInstance().getPiecesOfColour(colour);

		return moves;

	}

	@Override
	public long generateAttacks() {
		return generate();
	}

}
