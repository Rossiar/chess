package com.wishbone.pieces;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.moves.CastlingMove;
import com.wishbone.moves.Move;

public class WhiteKing extends King {

	public WhiteKing(Position startpos) {
		super(0, startpos);
		KING_CASTLE = 15L;
		QUEEN_CASTLE = 248L;
		value = 12;
	}

	@Override
	public Move move(Position to) {

		long toBB = (1L << to.getPosition());
		Move move = new Move(this, to, position);

		//if the move isn't legal
		if (Board.isEmpty(generate() & toBB)) {
			return null;
		}

		//find a capture if it exists
		Piece capture = Board.getInstance().getPiece(to);

		//move the piece there
		Position from = position;
		position = to;
		bitboard = toBB;

		//if we are capturing a piece, delete it from the Board.getInstance()
		if (capture != null) {
			Board.getInstance().pieces.remove(capture);
		}

		//castling
		if (!Board.isEmpty(Board.getInstance().getCastlingMoves() & toBB)) {
			Position newRookSquare = Position.ILLEGAL;
			Position oldRookSquare = Position.ILLEGAL;

			switch(to) {
			case B1:
				newRookSquare = Position.C1;
				oldRookSquare = Position.A1;
				break;
			case F1:
				newRookSquare = Position.E1;
				oldRookSquare = Position.H1;
				break;
			}

			//find and move the rook
			Piece rook = Board.getInstance().getPiece(oldRookSquare);
			Position rookFrom = rook.position;
			rook.position = newRookSquare;

			move = new CastlingMove(this, to, from,
					Board.getInstance().getCastlingMoves(), rook, rookFrom);
		}

		return move;
	}

	@Override
	protected long generateCastlingMoves() {

		if (position == Position.E1) {
			return 0L;
		}

		long moves = Board.getInstance().getCastlingMoves();
		long occupied = Board.getInstance().getOccupied();
		long threats = Board.getInstance().getAttacks(colour ^ 1);

		int kingCastleSquare = 1;
		int queenCastleSquare = 5;

		//mask moves so we are only working with the correct colour
		moves &= Board.RANK_1;

		/*
		 * If any pieces are in the way or any square is threatened
		 * then remove the king-side castling right.
		 */
		if (Long.bitCount(KING_CASTLE & occupied) > 2 |
				Board.isEmpty(threats & KING_CASTLE)) {
			moves &= ~(1L << kingCastleSquare);
		}

		/*
		 * If any pieces are in the way or any square is threatened
		 * then remove the queen-side castling right.
		 */
		if (Long.bitCount(QUEEN_CASTLE & occupied) > 2 |
				Board.isEmpty(threats & QUEEN_CASTLE)) {
			moves &= ~(1L << queenCastleSquare);
		}

		return moves;
	}

	@Override
	public long generateAttacks() {
		return generateMoves();
	}

}
