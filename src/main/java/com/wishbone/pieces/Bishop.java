package com.wishbone.pieces;

import com.wishbone.enums.Position;


public class Bishop extends SlidingPiece {

	public Bishop(int colour, Position startpos) {
		super(colour, startpos);
		value = 6 + colour;
	}

	@Override
	public long generate() {
		return generateDiagonalRays();
	}

}
