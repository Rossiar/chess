package com.wishbone.pieces;

import com.wishbone.enums.Position;


public class Rook extends SlidingPiece {

	public Rook(int colour, Position startpos) {
		super(colour, startpos);
		value = 8 + colour;
	}

	@Override
	public long generate() {
		return generateRays();
	}

}
