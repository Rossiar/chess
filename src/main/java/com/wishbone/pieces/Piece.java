package com.wishbone.pieces;

import java.util.ArrayList;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.moves.CaptureMove;
import com.wishbone.moves.Move;

public abstract class Piece {
	
	protected int value;
	protected long bitboard;
	protected Position position;
	protected int colour;
	
	public Piece(int colour, Position position) {
		this.colour = colour;
		this.position = position;
		this.bitboard = (1L << this.position.getPosition());
	}
	
	/**
	 * Moves this piece to the given square,
	 * provided that the move is legal. If the
	 * move is not legal then this method will
	 * return State.FAIL and no move will be made.
	 * 
	 * @param to - the square index to move to
	 * @return State.OK or State.FAIL
	 */
	public Move move(Position to) {

		if (!internalMove(to)) {
			return null;
		}
		
		//find a capture if it exists
		Piece capture = Board.getInstance().getPiece(to);

		//if we are capturing a piece, delete it from the Board.getInstance()
		if (capture != null) {
			Board.getInstance().pieces.remove(capture);
			return new CaptureMove(this, to, this.position, capture);
		}

		return new Move(this, to, this.position);
	}
	
	public boolean internalMove(Position to) {	
		long toBB = (1L << to.getPosition());
		
		//if the move isn't legal
		Board.printBoard(generate());
		
		Board.printBoard(toBB);
		
		if (Board.isEmpty(generate() & toBB)) {
			return false;
		}
		
		//move the piece there
		position = to;
		bitboard = toBB;
		return true;
	}
	
	/**
	 * Will get all the moves available
	 * 
	 * @return a list of moves this piece can make
	 */
	public Move[] getMoves() {
		ArrayList<Move> moves = new ArrayList<Move>();
		String boardString = Board.getBoardAsBinaryString(bitboard);

		for (int i = 0; i < boardString.length(); i++) {
			char c = boardString.charAt(63-i);
			if (c == '1') {
				Position to = Position.valueOf(i);
				Piece capture = Board.getInstance().getPiece(to);
				if (capture != null)
				{
					moves.add(new CaptureMove(this, Position.valueOf(i), this.position, capture));
				}
				moves.add(new Move(this, to, this.position));
			}
		}

		return moves.toArray(new Move[]{});
	}
	
	public abstract long generate();
	
	public abstract long generateAttacks();
	
	public Position getPos() {
		return position;
	}
	
	public long getBoard() {
		return bitboard;
	}
	
	public int getColour() {
		return colour;
	}
	
	public int getValue() {
		return value;
	}
	
	public boolean isColour(int colour) {
		return this.colour == colour;
	}
	
	@Override
	public String toString() {
		String c = colour == 0 ? "White" : "Black"; 
		
		return c + getClass().getSimpleName() + " at " + 
		position;
	}
}
