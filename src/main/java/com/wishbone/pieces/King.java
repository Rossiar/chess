package com.wishbone.pieces;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.moves.Move;

public abstract class King extends Piece {
	
	protected long KING_CASTLE;
	protected long QUEEN_CASTLE;

	public King(int colour, Position position) {
		super(colour, position);
	}

	@Override
	public long generate() {
		return generateCastlingMoves() | generateMoves();
	}
	
	@Override
	public abstract Move move(Position to);
	
	protected abstract long generateCastlingMoves();

	protected long generateMoves() {

		long ownPieces = Board.getInstance().getPiecesOfColour(colour);
		long moves = 0L;

		//up and down do not need to be masked
		moves |= bitboard << 8;
		moves |= bitboard >> 8;

		//northwest and southeast need to be masked on file A
		moves |= (bitboard << 7) & ~Board.FILE_A;
		moves |= (bitboard >> 9) & ~Board.FILE_A;

		//northeast and southwest need to be masked on file H
		moves |= (bitboard << 9) & ~Board.FILE_H;
		moves |= (bitboard >> 7) & ~Board.FILE_H;

		//stop bitboard moves wrapping on square 63
		if (Long.numberOfLeadingZeros(bitboard) == 0)
			moves &= ~Board.RANK_8;

		//left and right need to be masked against potential overflow on rank 8
		moves |= (bitboard >> 1) & ~Board.FILE_A;
		moves |= (bitboard << 1) & ~Board.FILE_H;

		//make sure we can't move over or capture our own pieces
		moves &= ~ownPieces;
		return moves;
	}

}
