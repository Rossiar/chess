package com.wishbone.pieces;

import com.wishbone.enums.Position;


public abstract class Pawn extends Piece {

	public Pawn(int colour, Position startpos) {
		super(colour, startpos);
	}

}
