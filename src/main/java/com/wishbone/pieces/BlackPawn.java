package com.wishbone.pieces;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.moves.EnpassantCaptureMove;
import com.wishbone.moves.EnpassantMove;
import com.wishbone.moves.Move;
import com.wishbone.moves.PromotionMove;

public class BlackPawn extends Pawn {

	public BlackPawn(Position startpos) {
		super(1, startpos);
		this.value = 3;
	}

	@Override
	public Move move(Position to) {
		//establish the bitboard of where we are moving to
		long toBB = (1L << to.getPosition());
		Move move = new Move(this, to, position);

		//if the move isn't legal
		if (Board.isEmpty(generate() & toBB)) {
			return null;
		}

		//enpassant
		if ((position.getPosition() - to.getPosition()) == 16) {
			Board.getInstance().setEnpassantTarget(to.before());
			move = new EnpassantMove(this, to, position, 
					Board.getInstance().getEnpassantTarget());
		}

		//enpassant capture
		if (Board.getInstance().getEnpassantTarget() == to) {

			//calculate if the captured piece is below
			Piece capture = Board.getInstance().getPiece(to.behind());

			//remove the piece from the Board.getInstance()
			Board.getInstance().pieces.remove(capture);

			move = new EnpassantCaptureMove(this, to, position, capture);
		}
		
		//move the piece there
		position = to;
		bitboard = toBB;

		//promotion
		if (!Board.isEmpty(bitboard & Board.RANK_1)) {
			Piece promotion = Board.getInstance().createPiece('q', position);
			move = new PromotionMove(this, to, position, promotion);
			
			Board.getInstance().pieces.add(promotion);
			Board.getInstance().pieces.remove(this);
		}

		return move;
	}

	@Override
	public long generate() {
		return generatePushes() | generateCaptures();
	}

	private long generatePushes() {

		long occupied = Board.getInstance().getOccupied();
		long push = 0L;
		long doublePush = 0L;

		push = bitboard >> 8;
		push &= ~occupied;
		doublePush = (push & Board.RANK_6) >> 8;

		push &= ~occupied;
		doublePush &= ~occupied;

		return (push | doublePush);
	}

	private long generateCaptures() {

		long leftCaptures, rightCaptures;
		long enpassantTarget = (1L << Board.getInstance().getEnpassantTarget().getPosition());

		//mask the rightmost file as the Board.getInstance() on a6 wraps around
		leftCaptures = (bitboard >> 9) & ~Board.FILE_A;

		//mask the leftmost file as the Board.getInstance() on h6 wraps around
		rightCaptures = (bitboard >> 7) & ~Board.FILE_H;

		//mask enpassant capture so we only capture enemy pieces
		enpassantTarget &= Board.RANK_3;

		long captures = leftCaptures | rightCaptures;
		boolean enpassant = false;

		//include enpassant capture
		if ((enpassantTarget & captures) > 0)
			enpassant = true;

		//include enemy pieces and exclude our own
		captures &= ~Board.getInstance().getPiecesOfColour(colour);
		captures &= Board.getInstance().getPiecesOfColour(colour ^ 1);

		if (enpassant)
			captures |= enpassantTarget;

		return (captures);
	}

	@Override
	public long generateAttacks() {
		return generateCaptures();
	}

}
