package com.wishbone.pieces;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.moves.CaptureMove;
import com.wishbone.moves.EnpassantCaptureMove;
import com.wishbone.moves.EnpassantMove;
import com.wishbone.moves.IllegalMove;
import com.wishbone.moves.Move;
import com.wishbone.moves.PromotionMove;

public class WhitePawn extends Pawn {

	public WhitePawn(Position startpos) {
		super(0, startpos);
		this.value = 2;
	}

	@Override
	public Move move(Position to) {

		// establish the bitboard of where the pawn is moving to
		long toBB = (1L << to.getPosition());

		// if the move isn't legal
		if (Board.isEmpty(generate() & toBB)) {
			return new IllegalMove();
		}

		// create a legal move
		Move move = new Move(this, to, position);
		
		// remove a piece from the board if it has been captured
		Piece capture = Board.getInstance().getPiece(to);
		if (capture != null)
		{
			Board.getInstance().pieces.remove(capture);
			move = new CaptureMove(move, capture);
		}

		// check if the pawn is moving 2 ranks forward (enpassant)
		if (this.position.before().before() == to) {
			Board.getInstance().setEnpassantTarget(to.behind());
			move = new EnpassantMove(this, to, position, 
					Board.getInstance().getEnpassantTarget());
		}

		// enpassant capture
		if (Board.getInstance().getEnpassantTarget() == to) {

			// calculate if the captured piece is below
			Piece enPassantCapture = Board.getInstance().getPiece(to.before());

			// remove the piece from the Board.getInstance()
			Board.getInstance().pieces.remove(enPassantCapture);

			move = new EnpassantCaptureMove(this, to, position, enPassantCapture);
		}

		// promotion
		if (!Board.isEmpty(bitboard & Board.RANK_8)) {
			Piece promotion = Board.getInstance().createPiece('Q', position);
			move = new PromotionMove(this, to, position, promotion);
			
			Board.getInstance().pieces.add(promotion);
			Board.getInstance().pieces.remove(this);
		}

		// move the pawn
		position = to;
		bitboard = toBB;

		return move;
	}

	@Override
	public long generate() {
		return generatePushes() | generateCaptures();
	}

	private long generatePushes() {

		long occupied = Board.getInstance().getOccupied();
		long push = 0L;
		long doublePush = 0L;

		// pawn pushes
		push = bitboard << 8;

		// ~ will invert occupied to give us the empty squares
		push &= ~occupied;

		// push twice then mask with row 3
		doublePush = (push & Board.RANK_3) << 8;

		push &= ~occupied;
		doublePush &= ~occupied;

		return (push | doublePush);
	}

	private long generateCaptures() {

		long leftCaptures, rightCaptures;
		long enpassantTarget = (1L << Board.getInstance().getEnpassantTarget().getPosition());

		// mask the rightmost file as the Board.getInstance() on a2 wraps around
		leftCaptures = (bitboard << 9) & ~Board.FILE_H;

		// mask the leftmost file as the Board.getInstance() on h2 wraps around
		rightCaptures = (bitboard << 7) & ~Board.FILE_A;

		// mask enpassant capture so we only capture enemy pieces
		enpassantTarget &= Board.RANK_6;

		long captures = leftCaptures | rightCaptures;
		boolean enpassant = false;

		// include enpassant capture
		if ((enpassantTarget & captures) > 0)
			enpassant = true;

		// include enemy pieces and exclude friendlies
		captures &= ~Board.getInstance().getPiecesOfColour(colour);
		captures &= Board.getInstance().getPiecesOfColour(colour ^ 1);

		if (enpassant)
			captures |= enpassantTarget;

		return (captures);
	}

	@Override
	public long generateAttacks() {
		return generateCaptures();
	}

}
