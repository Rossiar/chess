package test.unit.com.wishbone;

import static org.junit.Assert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.wishbone.Board;
import com.wishbone.enums.Position;

public class BoardTest {

	/**
	 * Iterate over every position on the board and insure that they are calculated correctly
	 */
	@Test
	public void testIndexToAlgebra() {

		for (int pos = 0; pos < 64; pos++)
		{
			assertThat(Board.indexToAlgebra(pos), is(Position.valueOf(pos).toString().toLowerCase()));
		}
	}

	/**
	 * Iterate over every position on the board and insure that they are calculated correctly
	 */
	@Test
	public void testAlgebraToIndex() {

		List<String> files = Arrays.asList(Board.ALBEBRA_FILES);
		Collections.reverse(files);
		for (int rank = 8; rank > 0; rank--)
		{
			for (String file : files)
			{
				String name = file.toUpperCase() + rank;
				assertThat(Position.valueOf(name).getPosition(), is(Board.algebraToIndex(name)));
			}
		}
	}

}
