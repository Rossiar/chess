package test.unit.com.wishbone.pieces;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.wishbone.Board;
import com.wishbone.enums.Position;
import com.wishbone.moves.CaptureMove;
import com.wishbone.moves.EnpassantMove;
import com.wishbone.moves.IllegalMove;
import com.wishbone.moves.Move;
import com.wishbone.pieces.BlackPawn;
import com.wishbone.pieces.Piece;
import com.wishbone.pieces.WhitePawn;

public class WhitePawnTest {

	/**
	 * Tests that a pawn can move to a legal position.
	 */
	@Test
	public void testMove() {
		Position start = Position.E2;
		Position to = Position.E3;
		
		WhitePawn wp = new WhitePawn(start);
		Move move = wp.move(to);
		
		assertThat(move instanceof IllegalMove, is(false));
		assertThat(move.getTo(), is(to.getPosition()));
		assertThat(move.getFrom(), is(start.getPosition()));
		assertThat(move.getPiece(), is((Piece) wp));
	}
	
	/**
	 * Tests that a pawn cannot move on top of an enemy piece
	 */
	@Test
	public void testMoveBlockingEnemy() {
		Position start = Position.E2;
		Position to = Position.E3;
		Board.getInstance().pieces.add(new BlackPawn(Position.E3));
		
		WhitePawn wp = new WhitePawn(start);
		Move move = wp.move(to);
		
		assertThat(move instanceof IllegalMove, is(true));
	}
	
	/**
	 * Tests that a pawn cannot move on top of a friendly piece
	 */
	@Test
	public void testMoveBlockingFriendly() {
		Position start = Position.E2;
		Position to = Position.E3;
		Board.getInstance().pieces.add(new WhitePawn(Position.E3));
		
		WhitePawn wp = new WhitePawn(start);
		Move move = wp.move(to);
		
		assertThat(move instanceof IllegalMove, is(true));
	}
	
	/**
	 * Tests that a pawn can capture an enemy piece.
	 */
	@Test
	public void testMoveCapture() {
		Position start = Position.E2;
		Position to = Position.D3;
		Piece capture = new BlackPawn(to);
		Board.getInstance().pieces.add(capture);
		
		Piece wp = new WhitePawn(start);
		Move move = wp.move(to);
		
		assertThat(move instanceof IllegalMove, is(false));
		assertThat(move instanceof CaptureMove, is(true));
		assertThat(move.getTo(), is(to.getPosition()));
		assertThat(move.getFrom(), is(start.getPosition()));
		assertThat(move.getPiece(), is(wp));
		assertThat(((CaptureMove) move).getCapture(), is(capture));
	}
	
	/**
	 * Tests that a pawn cannot capture a friendly piece.
	 */
	@Test
	public void testMoveCaptureFriendly() {
		Position start = Position.E2;
		Position to = Position.D3;
		Piece capture = new WhitePawn(to);
		Board.getInstance().pieces.add(capture);
		
		Piece wp = new WhitePawn(start);
		Move move = wp.move(to);
		
		assertThat(move instanceof IllegalMove, is(true));
	}
	
	/**
	 * Tests that a pawn can make an enpassant move
	 */
	@Test
	public void testMoveEnpassant() {
		Position start = Position.E2;
		Position to = Position.E4;
		
		WhitePawn wp = new WhitePawn(start);
		Move move = wp.move(to);
		
		Position enpassant = to.behind();
		
		assertThat(move instanceof IllegalMove, is(false));
		assertThat(move instanceof EnpassantMove, is(true));
		assertThat(((EnpassantMove) move).getEnpassant(), is(enpassant));
		assertThat(Board.getInstance().getEnpassantTarget(), is(enpassant));
		assertThat(move.getTo(), is(to.getPosition()));
		assertThat(move.getFrom(), is(start.getPosition()));
		assertThat(move.getPiece(), is((Piece) wp));
	}
	
	/**
	 * Tests that a pawn can make an enpassant move
	 */
	@Test
	public void testMoveEnpassantBlocking() {
		Position start = Position.E2;
		Position to = Position.E4;
		Board.getInstance().pieces.add(new WhitePawn(to.behind()));
		Board.getInstance().pieces.add(new BlackPawn(to));
		
		WhitePawn wp = new WhitePawn(start);
		Move move = wp.move(to);
		
		assertThat(move instanceof IllegalMove, is(true));
	}
	
	/**
	 * Tests that a pawn cannot move to an illegal position.
	 */
	@Test
	public void testMoveIllegal() {
		Position start = Position.E2;
		Position to = Position.D2;
		
		WhitePawn wp = new WhitePawn(start);
		Move move = wp.move(to);
		
		assertThat(move instanceof IllegalMove, is(true));
		assertThat(wp.getPos(), is(start));
	}

	@Test
	public void testGenerate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGenerateAttacks() {
		fail("Not yet implemented");
	}

}
